<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/khanh',function(){
	return view('admin.layout.index');
});
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix'=>'admin'],function(){

   
    Route::group(['prefix'=>'Chuyentien'],function(){
    	Route::get('getTransfer','TransferController@getTransfer')->name('getTransfer');
    	Route::post('postTransfer','TransferController@postTransfer')->name('postTransfer');
    });
	Route::group(['prefix'=>'User','middleware'=> 'checklogout'],function(){
		Route::get('list','UserController@index')->name('listuser');
		Route::get('add','UserController@create')->name('getadduser');
		Route::post('add','UserController@store')->name('postadduser');
		Route::get('getedit/{id}','UserController@edit')->name('getedituser');
		Route::post('postedit/{id}','UserController@update')->name('postedituser');
		Route::get('delete/{id}','UserController@destroy')->name('getDeluser');
	});
	Route::group(['prefix'=>'Wallet'],function(){
		Route::get('list','WalletController@index')->name('listwallet');
		Route::get('addwallet','WalletController@create')->name('getaddwallet');
		Route::post('postWallet','WalletController@store')->name('postaddwallet');
		Route::get('editwallet/{id}','WalletController@edit')->name('editwallet');
		Route::post('updatewallet/{id}','WalletController@update')->name('updatewallet');
		Route::get('deletewallet/{id}','WalletController@destroy')->name('Delwallet');
	});

});
 Auth::routes();
Route::get('/login','LoginController@getlogin')->middleware('checklogin')->name('login');
Route::post('/login','LoginController@postLogin')->name('postlogin');
Route::get('/logout','LoginController@getlogout')->name('logout');
Route::get('/dangki','LoginController@getdangki')->name('getdangki');
Route::post('/dangki','LoginController@postdangki')->name('postdangki');
