@extends('admin.layout.index')
@section('content')
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Post
          <small>Edit</small>
        </h1>
      </div>
      <table class="table">
        <thead>
         <tr align="center">
          <th style="text-align: center;">STT</th>
          <th style="text-align: center;">Name</th>
          <th style="text-align: center;">Số tiền</th>
          <th style="text-align: center;">Sửa ví</th>
          <th style="text-align: center;">Xóa ví</th>
        </tr>
      </thead>
      <tbody>
      @foreach($wallet as $wa)
       <tr class="odd gradeX" align="center">
        <td>{{$loop->iteration}}</td>
        <td>{{$wa->name}}</td>
        <td>{{$wa->money}}</td>
        
        <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="{{route('editwallet',$wa->id)}}">Sửa thông tin ví</a></td>
        <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="{{route('Delwallet',$wa->id)}}">Xóa ví</a></td>
      </tr>
      @endforeach
    </tbody>
  </table>

</div>



</div>
</div>

@stop