@extends('admin.layout.index')
@section('content')
<div id="page-wrapper" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Ví
                    <small>Thay đổi thông tin</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
               
                @if(session('thongbao'))
                <div class="alert alert-success">
                    {{session('thongbao')}}
                </div>
                @endif
                <form action="{{route('updatewallet',$wallet->id)}}" method="POST" >
                 @csrf()
                 <div class="form-group">
                    <label>Ten ví</label>
                    <input class="form-control" name="name" placeholder="Ten " value="{{$wallet->name}}" />
                </div>
                <div class="form-group">
                    <label>Số tiền</label>
                    <input class="form-control" name="money" placeholder="Money" value="{{$wallet->money}}"  />
                </div>
              
                
            <button type="submit" class="btn btn-primary">Thay đổi thông tin ví</button>
           
            
            <form>
            </div>
        </div>
     
    </div>
 
</div>


</div>
@stop