@extends('admin.layout.index')
@section('content')
<div id="page-wrapper" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">User
                    <small>add</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
               
                @if(session('thongbao'))
                <div class="alert alert-success">
                  {{session('thongbao')}}
                </div>
                @endif
                <form action="{{route('postaddwallet')}}" method="POST">
              {{csrf_field()}}
                 <div class="form-group">
                    <label>Ten</label>
                    <input class="form-control" name="name" placeholder="Ten "   />
                     @error('binamerthday')
                         <div class="alert alert-danger">{{ $message }}</div>
                     @enderror 
                </div>
                <div class="form-group">
                    <label>so tien</label>
                    <input class="form-control" type="number" name="money" placeholder="money"  />
                    @error('money')
                         <div class="alert alert-danger">{{ $message }}</div>
                     @enderror 
                </div>
                <div class="form-group">
            
               
              </div>
                <button type="submit" class="btn btn-primary">Add user</button>
               
                <a href="" class="btn btn-default" > Reset</a>
                <form>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

</div>
@stop