@extends('admin.layout.index')
@section('content')
<div id="page-wrapper" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Tài khoản
                    <small>Thay đổi thông tin</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
               
                @if(session('thongbao'))
                <div class="alert alert-success">
                    {{session('thongbao')}}
                </div>
                @endif
                <form action="{{route('postedituser',$user->id)}}" method="POST" enctype="multipart/form-data">
                 @csrf()
                 <div class="form-group">
                    <label>Ten</label>
                    <input class="form-control" name="name" placeholder="Ten " value="{{$user->name}}" />
                     @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                     @enderror
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" name="email" placeholder="email" value="{{$user->email}}"  />
                    @error('name')
                         <div class="alert alert-danger">{{ $message }}</div>
                     @enderror 
                </div>
                <div class="form-group">
                    <label>Số tài khoản</label>
                    <input class="form-control" disabled="code" name="code" placeholder="code" value="{{$user->code}}"  />
                   
                </div>
                <div class="form-group">
                    <label>Password </label>
                    <input class="form-control" type="password" name="password" placeholder="Nhap password" value="{{$user->password}}" disabled="disabled" />
                  
                </div>
                <div class="form-group">
                    <label>Anh</label>
                    <input class="form-control" type="file" name="images" placeholder="Body" value="{{$user->images}}" />
                    @error('images')
                         <div class="alert alert-danger">{{ $message }}</div>
                     @enderror 
                  </div>
                <div class="form-group">
                    <label>Birthday</label>
                    <input class="form-control" type="date" name="birthday" placeholder="Birthday" value="{{$user->birthday}}"  />
                    @error('birthday')
                         <div class="alert alert-danger">{{ $message }}</div>
                     @enderror 
                </div>
                
            <button type="submit" class="btn btn-primary">Thay đổi thông tin tài khoản</button>
            <form>
            </div>
        </div>
     
    </div>
 
</div>


</div>
@stop