@extends('admin.layout.index')
@section('content')
<div id="page-wrapper" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">User
                    <small>add</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                
                <form action="" method="POST">
                 @csrf()
                 <div class="form-group">
                    <label>Ten</label>
                    <input class="form-control" name="name" placeholder="Ten "   />
                     @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                     @enderror
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" name="email" placeholder="email"  />
                     @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                     @enderror     
                </div>
                <div class="form-group">
                    <label>Paasword </label>
                    <input class="form-control" type="password" name="password" placeholder="Nhap password"  />
                     @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                     @enderror 
                </div>
                  <div class="form-group">
                    <label>Paasword </label>
                    <input class="form-control" type="password" name="password" placeholder="Nhap password"  />
                     @error('password')
                        <div class="alert alert-danger">{{ $message }}</div>
                     @enderror 
                </div>
                  <div class="form-group">
                    <label>Password </label>
                    <input class="form-control" type="password" name="password" placeholder="Nhap password"  />
                     @error('password')
                        <div class="alert alert-danger">{{ $message }}</div>
                     @enderror
                </div>
                
                <button type="submit" class="btn btn-primary">Thêm tài khoản</button>
               
                <a href="" class="btn btn-default" > Reset</a>
                <form>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

</div>
@stop