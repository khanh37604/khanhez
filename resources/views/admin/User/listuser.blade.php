@extends('admin.layout.index')
@section('content')
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Tài khoản
          <small>Edit</small>

        </h1>
        <h4>
          <a href="{{route('getTransfer')}}">chuyển tiền</a>
        </h4>
       
      </div>
      <table class="table">
        <thead>
         <tr align="center">
          <th style="text-align: center;">STT</th>
          <th style="text-align: center;">Name</th>
          <th style="text-align: center;">Email</th>
          <th style="text-align: center;">Code</th>
          <th style="text-align: center;">Img</th>
          <th style="text-align: center;">Birthday</th>
          <th style="text-align: center;">Thay đổi thông tin</th>
         
        </tr>
      </thead>
      <tbody>
      @foreach($user as $us)
       <tr class="odd gradeX" align="center">
        <td>{{$loop->iteration}}</td>
        <td>{{$us->name}}</td>
        <td>{{$us->email}}</td>
        <td>{{$us->code}}</td>
       <td><img width="100px" height="100px" src="{{url('storage/public/avatar'.$us->images)}}" ></td>
        <td>{{$us->birthday}}</td>
        <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="{{route('getedituser',$us->id)}}">Thay đổi thông tin tài khoản</a></td>
        
      </tr>
      @endforeach
    </tbody>
  </table>

</div>



</div>
</div>

@stop