<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Wallet;
class Wallet extends Model
{
	protected $table = 'wallets' ;
	protected $fillable =['name','money','user_id'];

	
	public function user()
	{
		return $this->belongsTo(User::class);
		//vif 1 nguoi co the co hieu bai post,nen quan he 1-n,=>nguoc lai cua post laf belongsto
	}
}
