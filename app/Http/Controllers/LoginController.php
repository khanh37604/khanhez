<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Userrequest;

class LoginController extends Controller
{

    public function getlogin()

    {
    	return view('admin.login.login');
    }
    public function postlogin(Request $request)
    {
    	$data= [
    		'email' => $request->email,
    		'password' => $request->password
    	];
    	if (Auth::attempt($data)) {
    		return  redirect()->route('listuser');
    	}
    	else
    	{
    		echo "dang nhap khong hop le";
    	}
    }
     public function getdangki()
    {
    	return view('admin.login.dangki');
    }
    public function postdangki(Userrequest $request)
    {
    	$user = new User;
    	$user->name= $request->name;
    	$user->email= $request->email;
    	$user->code = mt_rand(0,10000000000);
    	$user->password= Hash::make($request->password);
    	$user->save();
    	return redirect()->route('getdangki')->with('thongbao','Ban đã đăng ký thành công');
    }
    public function getlogout()

    {
        Auth::logout();

        return redirect('/login');
    }
}
