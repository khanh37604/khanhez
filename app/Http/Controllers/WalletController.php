<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Wallet;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\WalletRequest;
use Illuminate\Support\Facades\DB;
class WalletController extends Controller
{
    public function index()
    {
    	$data['wallet'] = Wallet::all();
    	return view('admin.Wallet.listwallet',$data);
    }
    public function create()
    {
    		return view('admin.Wallet.addwallet');
    }
    public function store(WalletRequest $request)
        {
        	if(!auth()->user()->wallet) {
    			  $request->merge(['user_id'=>auth()->id()]);
    			 Wallet::create([
			    'name' => $request->name,
			    'money' => $request->money,
			   	'user_id' => $request->user_id
			]);
				return redirect()->route('listwallet')->with('thongbao','Ban đã thêm thành công');
    		}
    		else
    		{
    			return redirect()->back()->with('thongbao','Tài khoản đã tồn tại ví');
    		}  
    		

    	
    }
    public function edit($id)
    {
    	$wallet = Wallet::find($id);
    	 return view ('admin.Wallet.editwallet',compact('wallet'));
    }
    public function update(WalletRequest $request,$id)
    { 
    		//
    		
        
        			$wallet = Auth::user()->wallet;
		        		$wallet->update([
				            'money' => $request->money,
				            'name' => $request->name,
				        ]);
        		
		        	 return redirect()->route('listwallet')->with('thongbao', 'Sửa thông tin ví thành công');
		     
      

    }	
    public function destroy($id)
    {

    	  		Wallet::destroy($id);
    			return redirect()->route('listwallet')->with('thongbao','Ban đã thêm thành công');
    			
    }
   
}
