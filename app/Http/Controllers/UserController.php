<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\EditUser;
use App\User;
class UserController extends Controller
{
    public function index()
    {
    	 $data['user'] = User::all();
    	return view('admin.User.listuser',$data);
    }
    public function create()
    {
    	return view('admin.User.adduser');
    }
    public function store(Request $request)
    {
    	
    }
    public function edit($id)
    {
        $user = User::find($id);
        return view ('admin.User.edituser',compact('user'));

    } 
    public function update(EditUser $request,$id)
    {
      $user = User::find($id);
      $user->name = $request->name;
      $user->email=$request->email;
      $user->birthday=$request->birthday;
      if ($request->hasfile('images')) {
        $DelImages=$user->images;
        Storage::delete('/public/'.$DelImages);
        $user['images']=$request->file('images')->store('avatar','public');
      }
      $user->save();
      return redirect()->route('listuser')->with('thongbao','Update thanh cong');
    }
   
}
