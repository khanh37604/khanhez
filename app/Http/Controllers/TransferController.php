<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
Use App\Wallet;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\TransferWallet;
class TransferController extends Controller
{
    public function getTransfer()
    {
    	return view('admin.Chuyentien.chuyentien');
    }
    public function postTransfer(Request $request)
    {

        $value = $request->value;
        $myWallet = Auth::user()->wallet;
        $user = User::where('code', $request->code)->first();
        $wallet = $user->wallet;
        DB::transaction(function () use ($value, $myWallet, $wallet) {
            $myWallet->money -= $value;
            $myWallet->save();
            $wallet->money += $value;
            $wallet->save();

        });
        return redirect()->route('getTransfer')->with('thongbao','Bạn đã chuyển tiền thành công');
    }
}
