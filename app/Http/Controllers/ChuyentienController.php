<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
Use App\Wallet;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class ChuyentienController extends Controller
{
    public function getChuyen()
    {
    	return view('admin.Chuyentien.chuyentien');
    }
    public function postChuyen(Request $request)
    {

        $value = $request->value;
        $myWallet = Auth::user()->wallet;
        $user = User::where('code', $request->code)->first();
        $wallet = $user->wallet;
        DB::transaction(function () use ($value, $myWallet, $wallet) {
            $myWallet->money -= $value;
            $myWallet->save();
            $wallet->money += $value;
            $wallet->save();

        });
        return view('admin.Chuyentien.chuyentien');
    }

}


