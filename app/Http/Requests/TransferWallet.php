<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\User;
use App\Wallet;
class TransferWallet extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required',
            'money' => 'required'
           
        ];
    }
    public function messages()
    {
        return [
            'code.required' => 'Tài khoản không được để trống',
            'money.required' => 'Bạn chua nhập số tiền',
            'money.numeric' => 'Vui lòng nhập số tiền cần chuyển'
        ];
    }
}
