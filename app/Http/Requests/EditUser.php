<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use User;
class EditUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'images' => 'required|mimes:jpg,jpeg,png,gif,',
            'birthday' => 'required'
        ];
    }
    public function messages()
    {
        return [
             'name.required' => 'Name khong duoc de trong',
             'email.required' => 'Email khong duoc de trong',
             'images.required' => 'anh khong duoc de trong',
             'images.mimes' => 'anh chua dung dinh dang',
             'birthday.required' => 'Birthday không được để trống'
        ];
    }
}
