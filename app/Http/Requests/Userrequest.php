<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\User;
class Userrequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:5|max:15|',
        ];
    }
     public function messages()
    {
        return [
              'name.required' => 'Ban chua nhap ten',
              'email.required'=>'Tài khoản email không được để trống!',
              'email.email'=>'Tài khoản không đúng định dạng!',
              'password.required'=>'Mật khẩu không được để trống!',
              'password.min'=>'Mật khẩu phải ít nhất 5 ký tự!',
              'password.max'=>'Mật khẩu nhiều nhất 15 ký tự!',
        ];
    }
}
